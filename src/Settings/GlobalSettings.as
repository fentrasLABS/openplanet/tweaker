[Setting hidden]
bool Setting_EmbeddedFirstTime = true;

[Setting hidden]
bool Setting_EmbeddedInterface = true;

enum Shortcut
{
    Disabled,
    Hold,
    HoldReverse,
    Toggle
}