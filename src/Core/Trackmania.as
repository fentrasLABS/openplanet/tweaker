#if TMNEXT
class Vendor { }
class Mania : Game
{
    private void OverlayScaling()
    {
        auto overlays = view.Overlays;
        for (uint i = 0; i < overlays.Length; i++) {
            overlays[i].m_AdaptRatio = EHmsOverlayAdaptRatio(Setting_OverlayScaling);
        }
    }

    private void CheckLegacy()
    {
        if (!Setting_LegacyDefaults) return;
        CSystemConfigDisplay@ display = GetApp().Viewport.SystemConfig.Display;
        display.ZClip = CSystemConfigDisplay::EZClip::_ZClip_Disable;
        display.GeomLodScaleZ = 1.000f;
        Setting_LegacyDefaults = false;
        warn('Legacy Defaults have been applied. ZClip and GeomLodScaleZ are now reset.');
    }

    void VendorGame() override
    {
        CheckLegacy();
    }

    void AddVendorNods() override
    {
        @camera = Camera::GetCurrent();
        InitNods();
    }

    void ApplyVendorSettings() override
    {
#if SIG_REGULAR
        if (Setting_System_GeomLodScaleZ_Enabled) {
            view.SystemConfig.Display.GeomLodScaleZ = Setting_System_GeomLodScaleZ;
        }
#endif
    }

    void VendorRoutine() override
    {
#if SIG_REGULAR
        if (Setting_OverlayScaling != OverlayScaling::Default) {
            OverlayScaling();
        }
#endif
        if (initialised) {
            if (camera !is null) {
                if (Setting_ZClip) {
                    camera.FarZ = Setting_ZClipDistance;
#if SIG_REGULAR
                    if (Setting_ZClipAsync && app.Editor is null) {
                        cast<CVisionViewport>(view).AsyncRender = true;
                    }
#endif
                }
            }
        }
    }
}
#endif
