class InterfaceTab : Tab {
    string GetLabel(const string&in prefix = "") override { return Icons::WindowMaximize + prefix + " Interface"; }

    void Render() override
    {
#if TMNEXT
#if SIG_REGULAR
		UI::Markdown("## Overlay Scaling");

		if (UI::BeginCombo("Mode", tostring(Setting_OverlayScaling).Replace("_", " "))) {
			for (int i = -1; i < 10; i++) {
				if (tostring(OverlayScaling(i)) == tostring(i)) continue; // thanks to NaNInf
				if (UI::Selectable(tostring(OverlayScaling(i)).Replace("_", " "), false)) {
					Setting_OverlayScaling = OverlayScaling(i);
				}
			}
			UI::EndCombo();
		}

        if (UI::IsItemHovered()) {
			UI::BeginTooltip();
			UI::Text("Stretch all UI elements or resize to a different aspect ratio.");
			UI::Text("\n\\$ff0Experimental Feature");
			UI::Text("To revert default settings, choose \"Default\" option and restart the scene.");
			UI::Text("(e.g. if you're in the editor, go back to the menu and open the editor again)");
			UI::EndTooltip();
		}

		UI::Dummy(vec2(5, 5));
#endif
#endif

        UI::Markdown("## FPS Counter");

        Setting_FPS = UI::Checkbox("Enabled", Setting_FPS);

		if (UI::IsItemHovered()) {
			UI::BeginTooltip();
			UI::Text("Toggle simple FPS counter");
			UI::EndTooltip();
		}
    }
}