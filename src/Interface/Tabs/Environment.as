class EnvironmentTab : Tab {
#if !TMNEXT
    string GetLabel(const string&in prefix = "") override { return Icons::Tree + prefix + " Environment"; }

    void Render() override
    {
		UI::Markdown("## Background");

		Setting_BackgroundDouble = UI::Checkbox("Full Skydome", Setting_BackgroundDouble);

		if (UI::IsItemHovered()) {
			UI::BeginTooltip();
			UI::Text("Use double-sided skydome in environments other than Stadium");
			UI::EndTooltip();
		}
		
		UI::SameLine();
		if (!game.initialised) UI::BeginDisabled();
		if (UI::Button("Apply")) {
			game.FullSkyDomeToggle(Setting_BackgroundDouble);
		}
		if (!game.initialised) UI::EndDisabled();

    }
#endif
}